var express = require ('express'),
	fs = require ('fs'),
	imagemagick = require ('imagemagick'),
	_ = require ('underscore');

var minWidth = 2400,
	minHeight = 3200,
	staticRoot =  __dirname + '/public';

express ()
	.use (express.bodyParser ({
		keepExtensions: true,
		uploadDir: staticRoot + '/upload'
	}))
	.use (express.json ())
	.use (express.urlencoded ())
	.use (express.cookieParser ())
	.use (express.methodOverride ())
	.use (express.static (__dirname + '/public'))

	.post ('/upload', function (req, res, next) {
		var file = _.first (
			_.values (req.files)
		);

		function reject (error) {
			res.send (406, error);
		}

		function fulfill (result) {
			res.statusCode = 201;

			result.original =
				result.original
					.replace (staticRoot, '');

			result.preview =
				result.preview
					.replace (staticRoot, '');

			res.json (result);
		}

		function checkDimentions () {
			imagemagick.identify (file.path, function (error, result) {
				if (error) {
					reject (error);
				} else {
					if (result.width < minWidth && result.height < minHeight) {
						reject ('Image must be in vector or larger that ' + minWidth + 'x' + minHeight);
					} else {
						resize ();
					}
				}
			});
		}

		function resize () {
			var path = file.path.replace (/\./, '-small.');

			imagemagick.convert ([file.path, '-resize', '500x', path], function (error, result) {
				if (error) {
					reject (error);
				} else {
					fulfill ({
						original: file.path,
						preview: path
					});
				}
			});
		}

		function convert2png () {
			var path = file.path.replace (/\..*$/, '.png');

			imagemagick.convert ([
				'-colorspace', 'RGB', '-density', '300', '-background', 'transparent',
				file.path,
				'-background', 'transparent', '-flatten',
				'-resize', '500x',
				path
			], function (error) {
				if (error) {
					reject (error);
				} else {
					fulfill ({
						original: file.path,
						preview: path
					});
				}
			});
		}

		switch (file.type) {
			case 'image/png':
			case 'image/jpg':
			case 'image/jpeg':
				checkDimentions ();
				break;

			case 'application/postscript':
				convert2png ();
				break;

			default:
				reject ('Unkown file format ' + file.type);
		}
	})

	.listen (4444);
