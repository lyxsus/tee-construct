$ (function () {
	var states = ['create', 'options', 'description', 'finish'],
		currentStateIndex = 0;

	$ ('body').delegate ('.colors input[type=color]', 'change', function () {
		var $this = $ (this),
			$prev = $this.parent ().prev ();
		
		if (!$prev.is ('.custom')) {
			var $tmp = $ ('<div class="custom" />');
			$prev.after ($tmp);
			$prev = $tmp;
		}

		$prev.css ({
			'background-color': this.value
		})
	});

	$ ('.printable').click (function () {
		$ ('.elements .current')
			.removeClass ('current')
			.removeClass ('center');
		
		var $form = $ ('.tools .add-text.active form');
		
		if ($form.size ()) {
			$form [0].reset ();
		}
	});

	$ ('body').delegate ('.tabs', 'click', function (event) {
		var $this = $ (this);

		if ($ (event.target).parent ().is ('.active')) return false;

		$this
			.find ('> li')
				.toggleClass ('active');

		$this
			.parent ()
			.find ('> div')
				.toggleClass ('active');

		return false;
	});

	$ ('body').delegate ('input, select', 'keypress', function (event) {
		if (event.keyCode == 13) {
			$ (event.target)
				.blur ()
				.focus ();
			return false;
		} else {
			return true;
		}
	});

	$ ('body').delegate ('.canvas .flip', 'click', function () {
		$ (this)
			.parent ()
			.find ('> div > div')
			.toggleClass ('active');

		return false;
	});

	$ ('body').delegate ('.canvas .ui-delete', 'click', function () {
		$ (this)
			.parent ()
			.remove ();

		return false;
	});

	$ ('body').delegate ('.create .canvas .active .elements > div', 'click', function () {
		var $this = $ (this);

		$this.parent ().find ('div').removeClass ('current');
		$this.addClass ('current');
		updateCurrentElementForm ($this);

		return false;
	});

	$ ('body').delegate ('.add-text input[name=text]', 'keyup', function (event) {
		event.preventDefault ();
		event.stopPropagation ();

		getCurrentElement (true)
			.find ('span')
			.html (
				this.value
					.replace (/</g, '&lt;')
					.replace (/>/g, '&gt;')
					.replace (/$/, '&nbsp;')
			);

		checkBounds ();

		return false;
	});

	$ ('body').delegate ('.add-text select[name=text-font]', 'change', function () {
		getCurrentElement ()
			.css ('font-family', this.value);
	});

	$ ('body').delegate ('.add-text input[name=text-color]', 'change', function () {
		getCurrentElement ()
			.css ('color', this.value);
	});

	$ ('body').delegate ('.add-text select[name=outline]', 'change', function () {
		var $this = $ (this);

		if ($this.val () == 'Outline') {
			$ ('.add-text .outline').show ();
			getCurrentElement ()
				.css ('text-stroke-width', '1px');
		} else {
			getCurrentElement ()
				.css ('text-stroke-width', '0');
			$ ('.add-text .outline').hide ();
		}
	});

	$ ('body').delegate ('.add-text input[name=outline-color]', 'change', function () {
		var $this = $ (this);

		getCurrentElement ()
			.css ('text-stroke-color', this.value);
	});

	$ ('body').delegate ('.add-text input[name=outline-size]', 'change', function () {
		var $this = $ (this);

		getCurrentElement ()
			.css ('text-stroke-width', this.value + 'px');
	});

	$ ('body').delegate ('.add-text button[name=reflect-x], .add-text button[name=reflect-y]', 'click', function (event) {
		getCurrentElement ()
			.find ('span, img')
			.toggleClass (this.name);

		return false;
	});

	$ ('body').delegate ('.shirt-color .colors > div:not([class=picker])', 'click', function () {
		var color;

		repaintTee (
			parseRGB (
				color = $ (this).css ('background-color')
			)
		);

		$ ('.tools .options .option.tee ul li:first')
			.css ({
				'background-color': color
			})
			.removeClass ('empty');

		return false;
	});

	function repaintTee (color) {
		$ ('.canvas .active .base').each (function (i) {
			_repaintTee ($ (this), color);
		})
	}

	function _repaintTee ($image, color) {
		var canvas = $ ('#canvas') [0],
			ctx = canvas.getContext ('2d'),
			image = $image [0];

		$image.removeClass ('base');

		canvas.height = image.height;
		canvas.width = image.width;
		ctx.drawImage (image, 0, 0);

		var imgd = ctx.getImageData (0, 0, image.width, image.height),
			pix = imgd.data;

		for (var i = 0, n = pix.length; i <n; i += 4) {
			var r = pix [i],
				g = pix [i + 1],
				b = pix [i + 2];

			if(r || g || b){ 
				pix [i] = color.r;
				pix [i + 1] = color.g;
				pix [i + 2] = color.b;
			}
		}

		ctx.putImageData (imgd, 0, 0);

		image.src = canvas.toDataURL ('image/png');

		$image.addClass ('base');
	}

	function parseRGB (rgb) {
		var tmp = rgb.match (/(\d+), (\d+), (\d+)/);

		return {
			r: parseInt (tmp [1]),
			g: parseInt (tmp [2]),
			b: parseInt (tmp [3])
		};
	}

	// Duplicate
	$ ('body').delegate ('.add-text button[name=duplicate]', 'click', function () {
		var $current = getCurrentElement (),
			$cloned = $current.clone ();

		$current.removeClass ('current');

		$cloned.css ({
			top: 25 + parseInt ($cloned.css ('top') || 0),
			left: 25 + parseInt ($cloned.css ('left') || 0)
		});

		$current.after ($cloned);

		initializeElement ($cloned);

		$cloned
			.draggable ('destroy')
			.resizable ('destroy');

		initializeElement ($cloned);
		updateCurrentElementForm ($cloned);

		return false;
	});

	// Center
	$ ('body').delegate ('.add-text button[name=center]', 'click', function () {
		elementToCenter ();
		return false;
	});

	function elementToCenter ($el) {
		var $current = $el || getCurrentElement (),
			$elements = $current.parents ('.printable');

		$current.css ({
			left: Math.round (
					parseInt ($elements.css ('width')) / 2
				-
					parseInt ($current.css ('width')) / 2
			)
		});
	}

	function pushFront ($el) {
		var $current = $el || getCurrentElement (),
			$elements = $current.parent (),
			zIndex = 0;

		$elements.find ('> div').each (function () {
			var $this = $ (this);

			if (parseInt ($this.css ('z-index')) > zIndex) {
				zIndex = parseInt ($this.css ('z-index'));
			}
		});

		$current.css ({
			'z-index': zIndex + 1
		});

		return false;
	}

	function pushBehind ($el) {
		var $current = $el || getCurrentElement (),
			$elements = $current.parent (),
			zIndex = parseInt ($current.css ('z-index')),
			previousZIndex = 0,
			$previous;

		$elements.find ('> div').each (function () {
			var $this = $ (this),
				thisZIndex = parseInt ($this.css ('z-index'));

			if ((thisZIndex > previousZIndex) && (thisZIndex < zIndex)) {
				$previous = $this;
				previousZIndex = thisZIndex;
			}
		});

		if (previousZIndex) {
			$current.css ('z-index', previousZIndex);
			$previous.css ('z-index', zIndex);
		}
	}

	// Push behind
	$ ('body').delegate ('.add-text button[name=push-behind]', 'click', function () {
		setTimeout (pushBehind);
		return false;
	});

	$ ('.add-art')
		.bind ('dragenter dragover', false)

		.on ('drop', function (event) {
			event.preventDefault ();
			event.stopPropagation ();
			
			var files = event.originalEvent.dataTransfer.files;

			function progress (complete) {
				$ ('.add-art p').text ('Processing image ' + complete + '%');
			}

			var allowed = [
				'application/postscript'
			];

			for (var i = 0, file; i < files.length; i++) {
				file = files.item (0);


				if (/^image\//.test (file.type) || allowed.indexOf (file.type) !== -1) {
					var xhr = new XMLHttpRequest ();
					xhr.open ('POST', '/upload', true);
					// xhr.setRequestHeader ('Content-Type', 'multipart/form-data');

					console.log (file.name, file.type);
					
					xhr.onload = function () {
						switch (xhr.status) {
							case 201:
								progress (100);
								$ ('.add-art p').html ('Drop your image here<br />Add "JPG, PNG, EPS"');
								
								addArt (
									JSON.parse (
										xhr.response
									)
								);
								return;

							default:
								console.error (xhr.status, xhr.statusText, xhr.responseText);
								alert (xhr.responseText);
								$ ('.add-art p').text ('Drop your image here');
						}
					};

					xhr.upload.onprogress = function (event) {
						if (event.lengthComputable) {
							var complete = (event.loaded / event.total * 100 | 0);
							progress (complete);
						}
					};

					var formData = new FormData;
					formData.append (file.name, file);

					xhr.send (formData);
				} else {
					alert ('Unkown file format ' + file.type);
				}
				
			}
		})

		.on ('dragover', function (event) {
			event.preventDefault ();
			event.originalEvent.dataTransfer.dropEffect = 'copy';

			$ ('.add-art p').addClass ('dropover');
		})

		.on ('dragleave', function (event) {
			event.preventDefault ();

			$ ('.add-art p').removeClass ('dropover');
		});


	function changeStateIndex (index) {
		currentStateIndex = index;
		$ ('ul.nav li').removeClass ('active');
		$ ('ul.nav li:nth-child(' + (currentStateIndex + 1) + ')').addClass ('active');
		$ ('.tee-construct > div:first') [0].className = states [currentStateIndex];

		if (index == 2) {
			$ ('.tools .description').show ();
		} else {
			$ ('.tools .description').hide ();
		}

		if (index > 0) {
			$ ('.tee-construct .canvas .elements > div').removeClass ('current');
			$ ('.canvas').addClass ('prerender');
		} else {
			$ ('.canvas').removeClass ('prerender');
		}

		if (index == 1) {
			console.log ('clone all arts');
			var $elements;

			$ ('.tee-construct .canvas .elements').each (function (index) {
				if (index == 0) {
					$elements = $ (this);
				} else {
					$ (this).replaceWith (
						$elements.clone (true)
					);
				}
			});
		}

		if (index == 2) {
			// $ ('.rewind button[name=back]').hide ();
			$ ('.rewind button[name=next]').hide ();
			$ ('.rewind button[name=finish]').show ();
		} else {
			$ ('.rewind button[name=back]').show ();
			$ ('.rewind button[name=next]').show ();
			$ ('.rewind button[name=finish]').hide ();
		}
	}

	$ ('button[name=next]').click (function () {
		if (currentStateIndex == 3) return;

		changeStateIndex (++currentStateIndex);

		if (currentStateIndex == 3) {
			$ (this).css ('visibility', 'hidden');
		} else {
			$ ('button[name=back]').css ('visibility', 'visible');
		}
		return false;
	});

	$ ('button[name=finish]').click (function () {
		renderAll (function () {
			changeStateIndex (3);

			$ ('.rewind button').hide ();
			$ ('.tools .description').hide ();
			$ ('.canvas').hide ();
			$ ('ul.nav li').removeClass ('active');
			$ ('ul.nav li:last').addClass ('active');

			$ ('.preview')
				.html ('')
				.append (
					$ ('#output img:first')
						.clone ()
						.css ({
							width: '100%'
						})
				);

			// 
			$ ('.tools .options .option').each (function () {
				var $this = $ (this),
					$selected = $this.find ('ul:first li:not(.empty)'),
					type = this.className.replace (/^option /, ''),
					$output = $ ('.tee-construct .finish .tools .' + type + '-colors');

				// if ($selected.size ())
				if ($selected.size ()) {
					$output.show ();

					$output
						.find ('ul')
						.html ('');

					$selected.each (function () {
						$output
							.find ('ul')
							.append (
								$ (this).clone ()
							);
					});
				} else {
					$output.hide ();
				}
			})
		});

		return false;
	});

	$ ('button[name=back]').click (function () {
		if (currentStateIndex == 0) return;

		changeStateIndex (--currentStateIndex);

		if (currentStateIndex == 0) {
			$ (this).css ('visibility', 'hidden');
		} else {
			$ ('button[name=next]').css ('visibility', 'visible');
		}
		return false;
	});

	$ ('.canvas .holder').delegate ('.zoom', 'click', function (event) {
		$ ('.canvas .holder').toggleClass ('zoom');
		return false;
	});

	$ ('body').delegate ('.tools .options .colors > div:not([class=picker])', 'click', function () {
		var $this = $ (this),
			$option = $this.parents ('.option'),
			$slot = $option.find ('.empty:first').first ();

		if (!$slot.size ()) {
			$slot = $option.find ('li.active');
		}		

		if (!$slot.size ()) {
			$slot = $option.find ('li:last');
		}

		$slot
			.removeClass ('empty')
			.css ({
				'background-color': $this.css ('background-color')
			});

		return false;
	});

	$ ('body').delegate ('.tools .options .option > ul li:not([class=empty])', 'click', function () {
		if ($ (this).is ('.empty')) return;

		var className = $ (this).parents ('.option').attr ('class').replace (/^.* /, '');

		$ ('.canvas > .holder > .active')
			.removeClass ('active');

		$ ('.canvas > .holder > .' + className)
			.addClass ('active');


		repaintTee (
			parseRGB (
				$ (this).css ('background-color')
			)
		);
		return false;
	});

	$ ('button[name=yes]').click (function () {
		$ ('.tools .description input,textarea,select')
			.each (function () {
				$ (this).val ('');
			});

		$ ('.canvas').show ();
		$ ('.canvas .elements > div').remove ();

		$ ('.canvas .holder > .active').removeClass ('active');
		$ ('.canvas > .holder > :first').addClass ('active');

		changeStateIndex (0);
		return false;
	});

	$ ('button[name=no]').click (function () {
		alert ('TODO: What?')
		return false;
	});


	function getCurrentElement (activeOnly) {
		var $elements = $ ('.canvas .holder > .active .elements'),
			$el = $elements.find ('.current');

		if ($el.is ('.center')) {
			elementToCenter ($el);
		}

		if ($el.size ()) {
			return $el;
		}

		if (!activeOnly) {
			$el = $elements.find ('> div:first');

			if ($el.size ()) {
				$el.addClass ('current');
				updateCurrentElementForm ($el);
				return $el;
			}
		}
		
	
		$el = $ ('<div class="current"><span>Your text here&nbsp;</span></div>');
		$elements.append ($el);
		initializeElement ($el);

		$el.css ('top', 60);
		elementToCenter ($el);
		$el.addClass ('center');
		pushFront ($el);

		setTimeout (function () {
			updateCurrentElementForm ($el);
		});
		
		return $el;
	}

	function checkBounds () {
		var $elements = $ ('.canvas > .holder > .active .elements'),
			$els = $elements.find (' > div'),
			overflows = false,
			box = $elements.offset ();

		box.right = box.left + parseInt ($elements.css ('width'));
		box.bottom = box.top + parseInt ($elements.css ('height'));

		$els.each (function () {
			var $this = $ (this),
				offset = $this.offset ();

			if (box.left > offset.left) {
				overflows = true;
				return;
			}

			if (box.top > offset.top) {
				overflows = true;
				return;
			}

			if (box.bottom < (offset.top + parseInt ($this.css ('height')))) {
				overflows = true;
				return;
			}

			if (box.right < (offset.left + parseInt ($this.css ('width')))) {
				overflows = true;
				return;
			}
		});

		if (overflows) {
			$elements.parent ().addClass ('overflows');
		} else {
			$elements.parent ().removeClass ('overflows');
		}
		
	}

	function initializeElement (el) {
		var $el = $ (el);

		if ($el.find ('.ui-delete').size () == 0) {
			$el.append (
				$ ('<div class="ui-delete" />')
			);
		}

		if ($el.find ('.ui-drag').size () == 0) {
			$el.append (
				$ ('<div class="ui-drag" />')
			);
		}

		$el
			.draggable ({
				// containment: $el.parents ('.printable'),
				// handle: '.ui-drag',
				drag: function () {
					checkBounds ();
				}
			})

			.resizable ({
				// containment: $canvas,
				aspectRatio: true,
				handles: 'se',
				autoHide: false,
				alsoResize: $el.find ('img'),

				resize: function () {
					$ (this).css ({
						'font-size': Math.round (parseInt ($el.css ('height')) * 0.9)
					});

					checkBounds ();
				}
			})

			.rotatable ({
				handle: $ ('<div class="ui-rotate" />')
			});
	}

	function addArt (data) {
		var $elements = $ ('.canvas > .holder > .active .elements'),
			$el = $ ('<div />')
				.css ({
					top: 50,
					left: 50
				})
				.append (
					$ ('<img />')
						.attr ({
							'src': data.preview,
							'data-original': data.original
						})
				);

		$elements.append (
			$el
		);

		var width = parseInt ($el.css ('width')),
			height = parseInt ($el.css ('height'));
		
		// if ((width <= 2400) || (height <= 3200)) {
		if (false) {
			$el.remove ();
			alert ('Uploaded image size must be at least 2400x3200px');
			return false;
		} else {
			$el.find ('img').css ('width', '150px');
		}

		initializeElement ($el);
		updateCurrentElementForm ($el);

		$ ('.tools .shirt-editor .tabs li:first a').click ();
	}

	function updateCurrentElementForm ($el) {
		var $form = $ ('.add-text form'),
			$text = $el.find ('span');

		if ($text.size ()) {
			$form.find ('input[name=text]').val (
				$text.text ()
					.replace (/[ \t\r\n\xA0]+$/, '')
			);

			$form.find ('select[name=text-font]').val (
				$form.css ('font-family')
			);

			$form.find ('input[name=text-color]').val (
				colorToHex (
					$text.css ('color')
				)
			);

			var outlined = !!$text.css ('text-stroke-width') && ($text.css ('text-stroke-width') != '0px');

			$form.find ('select[name=outline]').val (
				outlined
					? 'Outline'
					: 'No outline'
			);

			if (outlined) {
				$form.find ('.outline').show ();
				
				$form.find ('input[name=outline-color]').val (
					rgb2hex ($el.css ('text-stroke-color'))
				);
				
				$form.find ('input[name=outline-size]').val (
					parseInt ($el.css ('text-stroke-width'))
				);
			} else {
				$form.find ('.outline').hide ();
			}


		}
	}

	function rgb2hex(rgb)
	{
		rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
		function hex(x) {
			return ("0" + parseInt(x).toString(16)).slice(-2);
		}
		return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
	}

	function colorToHex(color) {
	    if (color.substr(0, 1) === '#') {
	        return color;
	    }
	    var digits = /(.*?)rgb\((\d+), (\d+), (\d+)\)/.exec(color);

	    var red = parseInt(digits[2]);
	    var green = parseInt(digits[3]);
	    var blue = parseInt(digits[4]);

	    var rgb = blue | (green << 8) | (red << 16);
	    return digits[1] + '#' + rgb.toString(16);
	};

	$ ('.canvas .printable').each (function () {
		var $canvas = $ (this).parents ('.canvas');

		$ ('.elements > div', this).map (function (index, el) {
			initializeElement (el);
		});
			
	});

	$ ('.options .option').each (function () {
		var $colors = $ ('ul:first li', this);
		$colors.click (function () {
			$colors.removeClass ('active');
			$ (this).addClass ('active');
		});
	});

	setTimeout (function () {
		// changeStateIndex (1);
	// 	// $ ('button[name=finish]').click ();
	}, 0);


	function renderAll (callback) {
		var $canvas = $ ('.canvas'),
			$holders = $canvas.find ('.holder > div'),
			$activeHolder = $canvas.find ('.holder > div.active'),
			$output = $ ('#output');

		$canvas.addClass ('render');
		$holders.removeClass ('active');
		$output.html ('');
		$ ('.holder').removeClass ('zoom');

		$ ('#progress').addClass ('progress')
		
		var index = 0;
		function _renderNext () {
			var $holder = $ ($holders [index++]);
			
			if ($holder.size ()) {
				$holder.addClass ('active');

				$holder.find ('.elements > div')
					.each (function () {
						var $this = $ (this),
							$el = $this.find ('> :first');

						$el.css ({
							'transform': ($el.css ('transform') || '') + ' ' + $this.css ('transform')
						});
					});
				
				html2canvas ($ ('.canvas') [0], {
					onrendered: function (canvas) {
						$holder.removeClass ('active');
						$output.append (
							$ ('<img />')
								.attr ({
									src: canvas.toDataURL ('image/png')
								})
						);
						_renderNext ();
					}
				});
			} else {
				_finish ();
			}
		}

		function _finish () {
			$activeHolder.addClass ('active');
			$canvas.removeClass ('render');
			$ ('#progress').removeClass ('progress')


			if (callback) callback ();
		}

		_renderNext ();
	};

	// setTimeout (function () {
	// 	$ ('.shirt-color .colors div:nth-child(2)').click ();
	// });
	

	// $('#fontSelect').fontSelector({
 //        'hide_fallbacks' : true,
 //        'initial' : 'Courier New,Courier New,Courier,monospace',
 //        'selected' : function(style) { console.log ('font selected', style); },
 //        'fonts' : [
 //            'Arial,Arial,Helvetica,sans-serif',
 //            'Arial Black,Arial Black,Gadget,sans-serif',
 //            'Comic Sans MS,Comic Sans MS,cursive',
 //            'Courier New,Courier New,Courier,monospace',
 //            'Georgia,Georgia,serif',
 //            'Impact,Charcoal,sans-serif',
 //            'Lucida Console,Monaco,monospace',
 //            'Lucida Sans Unicode,Lucida Grande,sans-serif',
 //            'Palatino Linotype,Book Antiqua,Palatino,serif',
 //            'Tahoma,Geneva,sans-serif',
 //            'Times New Roman,Times,serif',
 //            'Trebuchet MS,Helvetica,sans-serif',
 //            'Verdana,Geneva,sans-serif',
 //            'Gill Sans,Geneva,sans-serif'
 //            ]
 //    });
});